#include <algorithm>
#include <array>
#include <cassert>
#include <cstring>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <numeric>
#include <omp.h>
#include <set>
#include <sstream>
#include <stdint.h>
#include <stdio.h>
#include <vector>

// Distribution[i] -> number of fermions occupying i-th harmonic oscillator (HO) shell
using Distribution = std::vector<uint16_t>;

namespace SU2 {
using LABEL = uint8_t;

void couple(int a2, int mult, int b2, std::vector<int>& multiplicities) {
   for (int c2 = abs(a2 - b2); c2 <= a2 + b2; c2 += 2) {
      multiplicities[c2] += mult;
   }
}
}  // namespace SU2

namespace U3 {
using LABELS = std::array<uint32_t, 3>;
using SPS = std::array<std::vector<uint32_t>, 3>;
enum { NZ, NX, NY };
};  // namespace U3

namespace UN {
using LABELS = std::vector<uint8_t>;
// using BASIS_STATE_WEIGHT_VECTOR = std::vector<uint8_t>;
using U3MULT_LIST = std::map<U3::LABELS, uint32_t>;
using SU3 = std::tuple<uint16_t, uint8_t, uint8_t>;
using SU3_VEC = std::vector<UN::SU3>;

using SU3xSU2 = std::tuple<uint16_t, uint8_t, uint8_t, SU2::LABEL>;
using SU3xSU2_VEC = std::vector<UN::SU3xSU2>;
}  // namespace UN

using IRREPS_FOR_A_FERMIONS = std::vector<std::vector<UN::SU3xSU2>>;
using SINGLE_SHELL_IRREPS = std::vector<IRREPS_FOR_A_FERMIONS>;

namespace SU3 {
inline int LM(const U3::LABELS& U3Labels) { return (U3Labels[U3::NZ] - U3Labels[U3::NX]); }
inline int MU(const U3::LABELS& U3Labels) { return (U3Labels[U3::NX] - U3Labels[U3::NY]); }
}  // namespace SU3

using MODEL_SPACE_DEFINITION = std::vector<
    std::map<std::array<uint8_t, 2>, std::map<uint8_t, std::set<std::array<uint8_t, 2>>>>>;

using GelfandRow = std::array<uint16_t, 3>;

enum { MULT, LM, MU, S2 };

int mult(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3) {
   int phase = lm1 + lm2 - lm3 - mu1 - mu2 + mu3;
   if (phase % 3 != 0) return 0;

   int redphase = phase / 3;
   int l1, l2, m1, m2, m3;

   if (phase >= 0) {
      l1 = lm1;
      l2 = lm2;
      m1 = mu1;
      m2 = mu2;
      m3 = mu3;
   } else {
      l1 = mu1;
      l2 = mu2;
      m1 = lm1;
      m2 = lm2;
      m3 = lm3;
      redphase = -redphase;
   }
   int invl1 = std::min(l1 - redphase, m2);
   if (invl1 < 0) return 0;

   int invl2 = std::min(l2 - redphase, m1);
   if (invl2 < 0) return 0;

   phase = redphase + m1 + m2 - m3;
   return std::max(std::min(phase, invl2) - std::max(phase - invl1, 0) + 1, 0);
}

void CoupleSU2Irreps(const std::vector<int>& spins_to_couple,
                     std::vector<int>& spin_multiplicities) {
   int max_SS = std::accumulate(spins_to_couple.begin(), spins_to_couple.end(), 0);
   spin_multiplicities.resize(max_SS + 1);
   std::fill(spin_multiplicities.begin(), spin_multiplicities.end(), 0);

   std::vector<int> tmp_spin_mult(max_SS + 1, 0);
   spin_multiplicities[spins_to_couple[0]] = 1;
   for (int i = 1; i < spins_to_couple.size(); ++i) {
      int ss2 = spins_to_couple[i];
      std::fill(tmp_spin_mult.begin(), tmp_spin_mult.end(), 0);
      for (int ss1 = 0; ss1 < spin_multiplicities.size(); ++ss1) {
         if (spin_multiplicities[ss1]) {
            SU2::couple(ss1, spin_multiplicities[ss1], ss2, tmp_spin_mult);
         }
      }
      spin_multiplicities = tmp_spin_mult;
   }
}

void CoupleTwoSU3Irreps(int lm1, int mu1, int mult1, int lm2, int mu2, int mult2,
                        std::vector<std::tuple<uint32_t, uint8_t, uint8_t>>& su3_mult) {
   su3_mult.reserve(3 * (lm1 + mu1 + lm2 + mu2));
   int lm2pmu2 = lm2 + mu2;
   int ix_max = std::min(mu1, lm2);
   for (int ix = 0; ix <= ix_max; ++ix) {
      int iy_max = std::min(lm1, lm2pmu2 - ix);
      for (int iy = 0; iy <= iy_max; ++iy) {
         int iz_min = std::max(0, mu2 - mu1 + ix - iy);
         int iz_max = std::min(mu2, lm2pmu2 - ix - iy);
         for (int iz = iz_min; iz <= iz_max; ++iz) {
            int lm3 = lm1 + lm2pmu2 - 2 * iy - ix - iz;
            int mu3 = mu1 - mu2 - ix + iy + 2 * iz;
            int ip = (2 * (mu3 - mu1) + lm3 - lm1 - lm2 + mu2) / 3;
            int in = (lm2pmu2 + lm2 - 2 * (lm3 - lm1) - mu3 + mu1) / 3;
            int irho = ix - std::max(std::max(0, -ip), in - lm1) + 1;
            if (irho != 1) {
               continue;
            }
            su3_mult.push_back(
                std::make_tuple(mult1 * mult2 * mult(lm1, mu1, lm2, mu2, lm3, mu3), lm3, mu3));
         }
      }
   }
}

void CoupleTwoSU3Irreps(int lm1, int mu1, int lm2, int mu2, std::vector<uint8_t>& su3_irreps) {
   su3_irreps.reserve(9 * (lm1 + mu1 + lm2 + mu2));
   int lm2pmu2 = lm2 + mu2;
   int ix_max = std::min(mu1, lm2);
   for (int ix = 0; ix <= ix_max; ++ix) {
      int iy_max = std::min(lm1, lm2pmu2 - ix);
      for (int iy = 0; iy <= iy_max; ++iy) {
         int iz_min = std::max(0, mu2 - mu1 + ix - iy);
         int iz_max = std::min(mu2, lm2pmu2 - ix - iy);
         for (int iz = iz_min; iz <= iz_max; ++iz) {
            int lm3 = lm1 + lm2pmu2 - 2 * iy - ix - iz;
            int mu3 = mu1 - mu2 - ix + iy + 2 * iz;
            int ip = (2 * (mu3 - mu1) + lm3 - lm1 - lm2 + mu2) / 3;
            int in = (lm2pmu2 + lm2 - 2 * (lm3 - lm1) - mu3 + mu1) / 3;
            int irho = ix - std::max(std::max(0, -ip), in - lm1) + 1;
            if (irho != 1) {
               continue;
            }
            su3_irreps.push_back(mult(lm1, mu1, lm2, mu2, lm3, mu3));
            su3_irreps.push_back(lm3);
            su3_irreps.push_back(mu3);
         }
      }
   }
}

/*
void CoupleTwoSU3Irreps(int lm1, int mu1, int mult1, int lm2, int mu2, int mult2,
                        std::vector<std::tuple<uint32_t, uint8_t, uint8_t>>& su3_mult) {
   for (int lm3 = 0; lm3 <= (lm1 + lm2 + mu1 + mu2); ++lm3) {
      for (int mu3 = 0; mu3 <= (lm1 + lm2 + mu1 + mu2); ++mu3) {
         int rhomax = mult(lm1, mu1, lm2, mu2, lm3, mu3);
         if (rhomax) {
            su3_mult.emplace_back(mult1 * mult2 * rhomax, lm3, mu3);
         }
      }
   }
}

void CoupleTwoSU3Irreps(int lm1, int mu1, int lm2, int mu2, std::vector<uint8_t>& su3_irreps) {
   for (int lm3 = 0; lm3 <= (lm1 + lm2 + mu1 + mu2); ++lm3) {
      for (int mu3 = 0; mu3 <= (lm1 + lm2 + mu1 + mu2); ++mu3) {
         int rhomax = mult(lm1, mu1, lm2, mu2, lm3, mu3);
         if (rhomax) {
            su3_irreps.push_back(rhomax);
            su3_irreps.push_back(lm3);
            su3_irreps.push_back(mu3);
         }
      }
   }
}
*/

void CoupleSU3Irreps(const UN::SU3_VEC& su3_to_couple,
                     std::vector<std::tuple<uint32_t, uint8_t, uint8_t>>& resulting_su3) {
   std::map<std::array<uint8_t, 2>, uint32_t> su3_mult_map;
   uint8_t lm1, mu1, lm2, mu2;
   uint32_t mult1, mult2;

   su3_mult_map = {{{std::get<LM>(su3_to_couple[0]), std::get<MU>(su3_to_couple[0])},
                    std::get<MULT>(su3_to_couple[0])}};

   for (size_t i = 1; i < su3_to_couple.size(); ++i) {
      std::tie(mult2, lm2, mu2) = su3_to_couple[i];
      std::vector<std::tuple<uint32_t, uint8_t, uint8_t>> mult_su3_tmp;
      for (const auto& su3_mult : su3_mult_map) {
         lm1 = su3_mult.first[0];
         mu1 = su3_mult.first[1];
         mult1 = su3_mult.second;
         CoupleTwoSU3Irreps(lm1, mu1, mult1, lm2, mu2, mult2, mult_su3_tmp);
      }

      su3_mult_map.clear();
      for (const auto& mult_su3 : mult_su3_tmp) {
         std::tie(mult1, lm1, mu1) = mult_su3;
         su3_mult_map[{lm1, mu1}] += mult1;
      }
   }

   for (const auto su3_mult : su3_mult_map) {
      resulting_su3.emplace_back(su3_mult.second, su3_mult.first[0], su3_mult.first[1]);
   }
}

void ShowDistributions(const std::vector<std::vector<Distribution>>& nhw_distributions) {
   for (int nhw = 0; nhw < nhw_distributions.size(); ++nhw) {
      std::cout << nhw << "hw" << std::endl;
      for (const auto& distribution : nhw_distributions[nhw]) {
         std::cout << "\t{";
         for (auto nferm : distribution) {
            std::cout << nferm << " ";
         }
         std::cout << "}" << std::endl;
      }
      std::cout << std::endl;
   }
}

void ShowSingleShellIrreps(const std::vector<std::vector<UN::SU3xSU2_VEC>>& n_A_irreps) {
   uint32_t num_irreps = 0, num_elems = 0;
   for (int n = 0; n < n_A_irreps.size(); ++n) {
      std::cout << n << ":" << std::endl;
      for (int A = 0; A < n_A_irreps[n].size(); ++A) {
         std::cout << "\tA:" << (A + 1) << std::endl;
         for (int i = 0; i < n_A_irreps[n][A].size(); ++i) {
            uint32_t mult;
            uint8_t lm, mu, S2;
            std::tie(mult, lm, mu, S2) = n_A_irreps[n][A][i];
            num_irreps += mult;
            std::cout << "\t\t" << mult << "(" << (int)lm << " " << (int)mu << ")" << (int)S2
                      << std::endl;
         }
         num_elems += n_A_irreps[n][A].size();
      }
   }
   std::cout << "Number of single shell U(N) > SU(3)xSU(2) irreps:" << num_irreps << std::endl;
   std::cout << "Number of elements in single shell U(N) > SU(3)xSU(2) irreps structure:"
             << num_elems << std::endl;
}

void GenerateDistributions(int Nmax, int A,
                           std::vector<std::vector<Distribution>>& nhw_distributions,
                           Distribution& max_occupancy) {
   // lowest_distribution: fermion distribution over HO shells for 0hw model space.
   // Example:
   //          HO shell n =   0    1    2     3     4     5 ... nmax
   // lowest_distribution = { 2,   6,   3,    0,    0,    0 ... ,0 }
   // The above configuration has 2 fermions in s-shell, 6 in p-shell and 3 in sd-shell
   Distribution lowest_distribution;

   // particles left to distribute
   int iNParticlesLeft = A;
   uint32_t n = 0;
   // This loop creates lowest_distribution, i.e. 0hw
   while (iNParticlesLeft > 0) {
      // number of fermions that can fit into n-th HO shell
      int N = (n + 1) * (n + 2);
      if ((iNParticlesLeft - N) > 0) {
         lowest_distribution.push_back(N);
         n++;
      } else {
         lowest_distribution.push_back(iNParticlesLeft);
      }
      iNParticlesLeft -= N;
   }
   // This is maximal HO shell that can contain maximally 1 particle
   int nmax = Nmax + n;
   // resize lowest_distribution up to nmax+1 elements
   lowest_distribution.resize(nmax + 1, 0);
   max_occupancy = lowest_distribution;

   nhw_distributions[0].push_back(std::move(lowest_distribution));
   // loop over nhw subspaces and for each subspace generate allowed distributions
   for (int nhw = 1; nhw <= Nmax; nhw++) {
      std::set<Distribution> set_distributions;
      // iterate over distributions spanning nhw-1 subspace
      for (const auto& seed_distribution : nhw_distributions[nhw - 1]) {
         for (int q = 0; q < nmax; q++) {
            // new_distribution contains nhw subspace distribution
            Distribution new_distribution(seed_distribution);
            if (new_distribution[q] && (new_distribution[q + 1] + 1) <= ((q + 2) * (q + 3))) {
               new_distribution[q] -= 1;
               new_distribution[q + 1] += 1;
               if (new_distribution[q + 1] > max_occupancy[q + 1]) {
                  max_occupancy[q + 1] = new_distribution[q + 1];
               }
               // STL set<> datastructure takes care of duplicities
               // Thus we need not explicitly check whether new_distribution
               // has been already encountered and stored
               set_distributions.insert(new_distribution);
            }
         }
      }
      // move nhw distributions to resulting structure of distributions
      nhw_distributions[nhw].insert(nhw_distributions[nhw].begin(), set_distributions.begin(),
                                    set_distributions.end());
   }
}

void GenerateU3Labels(GelfandRow gpr, const U3::SPS& ssps, U3::LABELS pp, UN::U3MULT_LIST& mU3lo) {
   size_t N = gpr[0] + gpr[1] + gpr[2] - 1;

   while (N > 0) {
      if (gpr[0]) {
         if (gpr[1] || gpr[2]) {
            GenerateU3Labels(
                {gpr[0] - 1, gpr[1], gpr[2]}, ssps,
                {pp[0] + 2 * ssps[0][N], pp[1] + 2 * ssps[1][N], pp[2] + 2 * ssps[2][N]}, mU3lo);
            if (gpr[2])
               GenerateU3Labels({gpr[0] - 1, gpr[1] + 1, gpr[2] - 1}, ssps,
                                {pp[0] + ssps[0][N], pp[1] + ssps[1][N], pp[2] + ssps[2][N]},
                                mU3lo);
         } else {
            gpr[0]--;
            pp[0] += 2 * ssps[0][N];
            pp[1] += 2 * ssps[1][N];
            pp[2] += 2 * ssps[2][N];
         }
      }

      if (gpr[1])
         if (gpr[2])
            GenerateU3Labels({gpr[0], gpr[1] - 1, gpr[2]}, ssps,
                             {pp[0] + ssps[0][N], pp[1] + ssps[1][N], pp[2] + ssps[2][N]}, mU3lo);
         else {
            gpr[1]--;
            pp[0] += ssps[0][N];
            pp[1] += ssps[1][N];
            pp[2] += ssps[2][N];
         }

      if (gpr[2]) gpr[2]--;

      N--;
   }

   auto temp = 2 * gpr[0] + gpr[1];
   pp[0] += temp * ssps[0][0];
   pp[1] += temp * ssps[1][0];
   pp[2] += temp * ssps[2][0];
   mU3lo[pp] += 1;
}

uint32_t GetMultiplicity(const U3::LABELS u3_labels, const UN::U3MULT_LIST& u3_mult_map) {
   auto u3_mult = u3_mult_map.find(u3_labels);
   if (u3_mult == u3_mult_map.end()) {
      std::cout << "Error: U3 irrep [" << u3_labels[0] << ", " << u3_labels[1] << ", "
                << u3_labels[2] << "] is not present in list of allowed U3 irreps";
      std::cout << std::endl;
      exit(0);
   }

   uint32_t f1 = u3_labels[0], f2 = u3_labels[1], f3 = u3_labels[2];
   uint32_t mult = u3_mult->second;

   u3_mult = u3_mult_map.find({f1 + 1, f2 + 1, f3 - 2});
   mult += (u3_mult == u3_mult_map.end()) ? 0 : u3_mult->second;

   u3_mult = u3_mult_map.find({f1 + 2, f2 - 1, f3 - 1});
   mult += (u3_mult == u3_mult_map.end()) ? 0 : u3_mult->second;

   u3_mult = u3_mult_map.find({f1 + 2, f2, f3 - 2});
   mult -= (u3_mult == u3_mult_map.end()) ? 0 : u3_mult->second;

   u3_mult = u3_mult_map.find({f1 + 1, f2 - 1, f3});
   mult -= (u3_mult == u3_mult_map.end()) ? 0 : u3_mult->second;

   u3_mult = u3_mult_map.find({f1, f2 + 1, f3 - 1});
   mult -= (u3_mult == u3_mult_map.end()) ? 0 : u3_mult->second;

   return mult;
}

void GetAllowed_UN_SU3xSU2_Irreps(uint32_t n, uint32_t A, const U3::SPS& ShellSPS,
                                  UN::SU3xSU2_VEC& AllowedIrreps) {
   const uint32_t N = (n + 1) * (n + 2) / 2;
   if (2 * N < A) {
      std::cout << "Error: only " << 2 * N << " states available for " << A << " particles!"
                << std::endl;
      exit(-1);
   }

   UN::LABELS U2Labels(2);
   SU2::LABEL S2min = !(A % 2) ? 0 : 1;  // if A = 2k => S2min = 0; S2min = 1 otherwise
   SU2::LABEL S2max = A;
   for (SU2::LABEL S2 = S2min; S2 <= S2max; S2 += 2) {
      // U2Labels[0] also equals to the number of spin-up fermions
      U2Labels[0] = (A + S2) / 2;
      U2Labels[1] = (A - S2) / 2;

      if (U2Labels[0] > N) {
         continue;
      }

      //	Iterate over U(3) labels and calculate multiplicity Mult (corresponds to
      //	alpha in [f] alpha (lm mu)S).  if Mult > 0 => add a given SU(3)xSU(2) irrep into
      // SU3xSU2Labels
      UN::U3MULT_LIST mU3_mult;
      // UN::BASIS_STATE_WEIGHT_VECTOR Weight(UNLabels.size());

      GenerateU3Labels({U2Labels[1], U2Labels[0] - U2Labels[1], N - U2Labels[0]}, ShellSPS,
                       {0, 0, 0}, mU3_mult);

      //	iterate over all [N_{z}, N_{x}, N_{y}] labels spanning U(N) irrep
      for (const auto& u3labels_mult : mU3_mult) {
         U3::LABELS U3Labels(u3labels_mult.first);
         if (U3Labels[U3::NZ] >= U3Labels[U3::NX] &&
             U3Labels[U3::NX] >= U3Labels[U3::NY])  // this could be U(3) irrep ... let's check it
         {
            uint32_t u3_mult = GetMultiplicity(U3Labels, mU3_mult);
            if (u3_mult) {
               AllowedIrreps.emplace_back(u3_mult, SU3::LM(U3Labels), SU3::MU(U3Labels), S2);
            }
         }
      }
   }
}

void GenerateU3SPS(int n, U3::SPS& ShellSPS) {
   for (int k = 0; k <= n; k++) {
      uint32_t nz = n - k;
      for (int nx = k; nx >= 0; nx--) {
         ShellSPS[U3::NX].push_back(nx);
         ShellSPS[U3::NY].push_back(n - nz - nx);
         ShellSPS[U3::NZ].push_back(nz);
      }
   }
   //   std::cout << "Shell n:" << n << " #sps states:" << ShellSPS[U3::NX].size() << std::endl;
}

void GenerateSingleShellIrreps(const Distribution& max_occupancy,
                               std::vector<std::vector<UN::SU3xSU2_VEC>>& n_A_irreps) {
   std::vector<std::pair<int, int>> pairs;
   for (int n = 0; n < max_occupancy.size(); ++n)
      for (int A = 1, maxA = max_occupancy[n]; A <= maxA; ++A) pairs.emplace_back(n, A);

   std::map<std::pair<int, int>, UN::SU3xSU2_VEC> n_A_irreps_map;

#pragma omp parallel
   {
      U3::SPS ShellSPS;  // set of sps (Nz, Nx, Ny) for harmonic oscillator shell n
      std::map<std::pair<int, int>, UN::SU3xSU2_VEC> n_A_irreps_map_local;

#pragma omp for schedule(dynamic, 1)
      for (int k = 0; k < pairs.size(); k++) {
         int n = pairs[k].first;
         int A = pairs[k].second;

         for (auto& ssps : ShellSPS) ssps.clear();
         GenerateU3SPS(n, ShellSPS);

         UN::SU3xSU2_VEC irreps;
         std::cout << "n:" << n << " A:" << A << std::endl;
         if (A == 1) {
            irreps.emplace_back(1, n, 0, 1);
         } else if (A == (n + 1) * (n + 2)) {
            irreps.emplace_back(1, 0, 0, 0);
         } else {
            GetAllowed_UN_SU3xSU2_Irreps(n, A, ShellSPS, irreps);
         }

         n_A_irreps_map_local[std::make_pair(n, A)] = std::move(irreps);
      }
#pragma omp critical
      n_A_irreps_map.insert(std::begin(n_A_irreps_map_local), std::end(n_A_irreps_map_local));
   }

   n_A_irreps.resize(max_occupancy.size());
   for (int n = 0; n < max_occupancy.size(); ++n)
      for (int A = 1, maxA = max_occupancy[n]; A <= maxA; ++A)
         n_A_irreps[n].push_back(std::move(n_A_irreps_map[std::make_pair(n, A)]));
}

void GenerateSU3xSU2Irreps(
    int max2S, const std::vector<std::vector<Distribution>>& nhw_distributions,
    const std::vector<std::vector<UN::SU3xSU2_VEC>>& n_A_irreps,
    std::vector<std::vector<std::pair<std::array<uint8_t, 2>, std::vector<uint32_t>>>>&
        nhw_su3su2_mult) {
   UN::SU3_VEC su3_to_couple;
   su3_to_couple.reserve(255);

   std::vector<int> spins_to_couple;
   spins_to_couple.resize(255);

   std::vector<std::tuple<uint32_t, uint8_t, uint8_t>> resulting_mult_su3;
   resulting_mult_su3.reserve(6 * 1024);

   std::vector<int> spin_multiplicities;
   spin_multiplicities.reserve(255);

   for (size_t nhw = 0; nhw < nhw_distributions.size(); ++nhw) {
      std::cout << nhw << "hw ...";
      std::cout.flush();
      std::map<std::array<uint8_t, 2>, std::vector<uint32_t>> su3su2_mult_map;
      for (const auto& distr : nhw_distributions[nhw]) {
         std::vector<std::reference_wrapper<const UN::SU3xSU2_VEC>> allowed_su3xsu2;
         for (size_t n = 0; n < distr.size(); ++n) {
            uint32_t A = distr[n];
            if (A) {
               allowed_su3xsu2.push_back(std::cref(n_A_irreps[n][A - 1]));
            }
         }

         // Now we generate all possible combinations of coupling out of allowed_su3xsu2
         size_t NIrrepsSets = allowed_su3xsu2.size();
         // Fill vector with 1 => vElemsPerChange[0] = 1;
         std::vector<unsigned> vElemsPerChange(NIrrepsSets, 1);
         std::vector<size_t> vNSizes(NIrrepsSets);
         // uNAllowedCombinations = number of all possible coupling combinations
         // uNAllowedCombinations = allowed_su3xsu2[0].size()* ...
         // *allowed_su3xsu2[NIrrepsSets].size()
         unsigned uNAllowedCombinations = 1;

         for (size_t i = 0; i < NIrrepsSets; i++) {
            vNSizes[i] = (allowed_su3xsu2[i]).get().size();
            uNAllowedCombinations *= vNSizes[i];
         }

         for (size_t i = 1; i < NIrrepsSets; i++) {
            // if i == 0 => vElemsPerChange[0]=1 due to constructor we used
            vElemsPerChange[i] = vElemsPerChange[i - 1] * vNSizes[i - 1];
         }

         // iterate through all possible combinations of
         for (unsigned index = 0; index < uNAllowedCombinations; index++) {
            // in each step create a set of SU3 and SU2 irreps to couple
            su3_to_couple.resize(0);
            spins_to_couple.resize(0);
            for (size_t i = 0; i < NIrrepsSets; i++) {
               size_t iElement = (index / vElemsPerChange[i]) % vNSizes[i];
               su3_to_couple.emplace_back(std::get<MULT>((allowed_su3xsu2[i].get())[iElement]),
                                          std::get<LM>((allowed_su3xsu2[i].get())[iElement]),
                                          std::get<MU>((allowed_su3xsu2[i].get())[iElement]));
               spins_to_couple.push_back(std::get<S2>((allowed_su3xsu2[i].get())[iElement]));
            }
            resulting_mult_su3.resize(0);
            CoupleSU3Irreps(su3_to_couple, resulting_mult_su3);

            spin_multiplicities.resize(0);
            CoupleSU2Irreps(spins_to_couple, spin_multiplicities);

            for (const auto& mult_su3 : resulting_mult_su3) {
               uint8_t lm, mu;
               uint32_t mult;
               std::tie(mult, lm, mu) = mult_su3;

               auto su3su2_mult_iter = su3su2_mult_map.find({lm, mu});
               if (su3su2_mult_iter == su3su2_mult_map.end()) {
                  std::vector<uint32_t> zero_multiplicities(max2S + 1, 0);  // [0] ... [max2S]
                  su3su2_mult_iter =
                      (su3su2_mult_map.insert(std::make_pair(std::array<uint8_t, 2>({lm, mu}),
                                                             std::move(zero_multiplicities))))
                          .first;
               }

               std::vector<uint32_t>& multiplicities = su3su2_mult_iter->second;
               for (uint8_t ss = 0; ss < spin_multiplicities.size(); ss++) {
                  if (spin_multiplicities[ss]) {
                     multiplicities[ss] += mult * spin_multiplicities[ss];
                  }
               }
            }
         }
      }

      for (const auto& su3su2_mult : su3su2_mult_map) {
         nhw_su3su2_mult[nhw].push_back(su3su2_mult);
      }
      std::cout << "Done" << std::endl;
   }
}

void CoupleProtonNeutronSpins(const std::vector<uint32_t>& spin_mult_proton,
                              const std::vector<uint32_t>& spin_mult_neutron,
                              std::vector<uint64_t>& spin_mult) {
   assert(spin_mult.size() == spin_mult_proton.size() + spin_mult_neutron.size() - 1);

   for (size_t ssp = 0; ssp < spin_mult_proton.size(); ++ssp) {
      uint32_t multp = spin_mult_proton[ssp];
      if (multp) {
         for (size_t ssn = 0; ssn < spin_mult_neutron.size(); ++ssn) {
            // we are multiplying two uint32_t numbers
            // result can be uint64_t
            uint64_t mult = multp * spin_mult_neutron[ssn];
            if (mult) {
               for (size_t ss = abs(ssp - ssn); ss <= ssp + ssn; ss += 2) {
                  spin_mult[ss] += mult;
               }
            }
         }
      }
   }
}

void ShowFermIrreps(
    const std::vector<std::vector<std::pair<std::array<uint8_t, 2>, std::vector<uint32_t>>>>&
        nhw_irreps) {
   for (size_t nhw = 0; nhw < nhw_irreps.size(); ++nhw) {
      std::cout << nhw << "hw:" << std::endl;
      for (const auto& su3_su2mult : nhw_irreps[nhw]) {
         int lm = su3_su2mult.first[0];
         int mu = su3_su2mult.first[1];
         const std::vector<uint32_t>& ss_mult = su3_su2mult.second;
         for (size_t ss = 0; ss < ss_mult.size(); ++ss) {
            if (ss_mult[ss]) {
               std::cout << "\t(" << lm << " " << mu << ") SS:" << ss << "\t" << ss_mult[ss]
                         << std::endl;
            }
         }
      }
   }
}

void CoupleProtonsNeutrons(
    int maxSS,
    const std::vector<std::vector<std::pair<std::array<uint8_t, 2>, std::vector<uint32_t>>>>&
        nhw_proton_irreps,
    const std::vector<std::vector<std::pair<std::array<uint8_t, 2>, std::vector<uint32_t>>>>&
        nhw_neutron_irreps,
    std::vector<std::map<std::array<uint8_t, 2>, std::vector<uint64_t>>>& nhw_su3su2_mult_maps) {
   std::vector<uint8_t> su3_irreps;
   su3_irreps.reserve(24576);

   std::vector<uint64_t> ss_mult(maxSS + 1, 0);

   for (size_t nhw = 0; nhw < nhw_proton_irreps.size(); nhw++) {
      std::cout << nhw << "hw ... ";
      std::cout.flush();
      for (size_t nphw = 0; nphw <= nhw; nphw++) {
         size_t nnhw = nhw - nphw;
         for (const auto& su3_su2mult_proton : nhw_proton_irreps[nphw]) {
            uint8_t lmp = su3_su2mult_proton.first[0];
            uint8_t mup = su3_su2mult_proton.first[1];
            const auto& ssp_mult = su3_su2mult_proton.second;
            for (const auto& su3_su2mult_neutron : nhw_neutron_irreps[nnhw]) {
               uint8_t lmn = su3_su2mult_neutron.first[0];
               uint8_t mun = su3_su2mult_neutron.first[1];
               const auto& ssn_mult = su3_su2mult_neutron.second;

               su3_irreps.resize(0);
               CoupleTwoSU3Irreps(lmp, mup, lmn, mun, su3_irreps);

               std::fill(ss_mult.begin(), ss_mult.end(), 0);
               CoupleProtonNeutronSpins(ssp_mult, ssn_mult, ss_mult);

               for (size_t i = 0; i < su3_irreps.size(); i += 3) {
                  uint8_t rhomax = su3_irreps[i];
                  uint8_t lm = su3_irreps[i + 1];
                  uint8_t mu = su3_irreps[i + 2];

                  auto su3_su2mult_iter = nhw_su3su2_mult_maps[nhw].find({lm, mu});
                  if (su3_su2mult_iter == nhw_su3su2_mult_maps[nhw].end()) {
                     std::transform(ss_mult.begin(), ss_mult.end(), ss_mult.begin(),
                                    std::bind2nd(std::multiplies<uint64_t>(), rhomax));
                     nhw_su3su2_mult_maps[nhw][{lm, mu}] = ss_mult;
                  } else {
                     auto& spin_mult_new = su3_su2mult_iter->second;
                     for (size_t i = 0; i < spin_mult_new.size(); ++i) {
                        spin_mult_new[i] += rhomax * ss_mult[i];
                     }
                  }
               }
            }
         }
      }
      std::cout << "Done" << std::endl;
   }
}

void CoupleProtonNeutronSpins(const std::vector<uint32_t>& spin_mult_proton,
                              const std::vector<uint32_t>& spin_mult_neutron,
                              std::vector<std::array<uint8_t, 3>>& sspssnss,
                              std::vector<uint64_t>& multiplicities) {
   for (uint8_t ssp = 0; ssp < spin_mult_proton.size(); ++ssp) {
      uint32_t multp = spin_mult_proton[ssp];
      if (multp) {
         for (uint8_t ssn = 0; ssn < spin_mult_neutron.size(); ++ssn) {
            // we are multiplying two uint32_t numbers
            // result can be uint64_t
            // TODO: check for integer overflow
            uint64_t mult = multp * spin_mult_neutron[ssn];
            if (mult) {
               for (uint8_t ss = abs(ssp - ssn); ss <= ssp + ssn; ss += 2) {
                  sspssnss.emplace_back(std::array<uint8_t, 3>({ssp, ssn, ss}));
                  multiplicities.push_back(mult);
               }
            }
         }
      }
   }
}

void CoupleProtonsNeutrons(
    int maxSS,
    const std::vector<std::vector<std::pair<std::array<uint8_t, 2>, std::vector<uint32_t>>>>&
        nhw_proton_irreps,
    const std::vector<std::vector<std::pair<std::array<uint8_t, 2>, std::vector<uint32_t>>>>&
        nhw_neutron_irreps,
    const std::vector<int>& nhw_spaces,
    std::vector<std::map<std::array<uint8_t, 3>, std::map<std::array<uint8_t, 2>, uint64_t>>>&
        nhw_map_spins_to_map_su3_to_mult) {
   std::vector<uint8_t> su3_irreps;
   su3_irreps.reserve(24576);
   std::vector<std::array<uint8_t, 3>> spins;
   spins.reserve(1024);
   std::vector<uint64_t> mult;
   mult.reserve(1024);

   for (size_t inhw = 0; inhw < nhw_spaces.size(); ++inhw) {
      int nhw = nhw_spaces[inhw];
      std::cout << nhw << "hw ... ";
      std::cout.flush();
      for (size_t nphw = 0; nphw <= nhw; nphw++) {
         size_t nnhw = nhw - nphw;
         for (const auto& su3_su2mult_proton : nhw_proton_irreps[nphw]) {
            uint8_t lmp = su3_su2mult_proton.first[0];
            uint8_t mup = su3_su2mult_proton.first[1];
            const auto& ssp_mult = su3_su2mult_proton.second;
            for (const auto& su3_su2mult_neutron : nhw_neutron_irreps[nnhw]) {
               uint8_t lmn = su3_su2mult_neutron.first[0];
               uint8_t mun = su3_su2mult_neutron.first[1];
               const auto& ssn_mult = su3_su2mult_neutron.second;

               su3_irreps.resize(0);
               CoupleTwoSU3Irreps(lmp, mup, lmn, mun, su3_irreps);

               spins.resize(0);
               mult.resize(0);
               CoupleProtonNeutronSpins(ssp_mult, ssn_mult, spins, mult);

               for (size_t iss = 0; iss < spins.size(); ++iss) {
                  auto spins_to_map_su3_to_mult_iter =
                      nhw_map_spins_to_map_su3_to_mult[inhw].find(spins[iss]);
                  if (spins_to_map_su3_to_mult_iter ==
                      nhw_map_spins_to_map_su3_to_mult[inhw].end()) {
                     std::map<std::array<uint8_t, 2>, uint64_t> map_su3_to_mult;
                     for (size_t j = 0; j < su3_irreps.size(); j += 3) {
                        uint8_t rhomax = su3_irreps[j];
                        uint8_t lm = su3_irreps[j + 1];
                        uint8_t mu = su3_irreps[j + 2];
                        map_su3_to_mult[{lm, mu}] = rhomax * mult[iss];
                     }
                     nhw_map_spins_to_map_su3_to_mult[inhw].emplace(spins[iss], map_su3_to_mult);
                  } else {
                     auto& map_su3_to_mult = spins_to_map_su3_to_mult_iter->second;
                     for (size_t j = 0; j < su3_irreps.size(); j += 3) {
                        uint8_t rhomax = su3_irreps[j];
                        uint8_t lm = su3_irreps[j + 1];
                        uint8_t mu = su3_irreps[j + 2];
                        map_su3_to_mult[{lm, mu}] += rhomax * mult[iss];
                     }
                  }
               }
            }
         }
      }
      std::cout << "Done" << std::endl;
   }
}

inline int kmax(int lm, int mu, const int L) {
   return (std::max(0, (lm + mu + 2 - L) / 2) - std::max(0, (lm + 1 - L) / 2) -
           std::max(0, (mu + 1 - L) / 2));
}

void computeDims(const std::array<uint8_t, 3>& lmmuss, std::vector<uint32_t>& dims) {
   uint8_t lm = lmmuss[0];
   uint8_t mu = lmmuss[1];
   uint8_t ss = lmmuss[2];

   dims.resize(2 * lm + 2 * mu + ss + 1);
   std::fill(dims.begin(), dims.end(), 0);
   for (uint32_t l = 0; l <= lm + mu; ++l) {
      int k0max = kmax(lm, mu, l);
      if (k0max) {
         for (size_t jj = abs(2 * l - ss); jj <= (2 * l + ss); jj += 2) {
            dims[jj] += k0max;
         }
      }
   }
}

void ComputeDimensions(const std::vector<std::map<std::array<uint8_t, 2>, std::vector<uint64_t>>>&
                           nhw_su3su2_mult_maps,
                       std::vector<std::array<uint64_t, 256>>& nhw_dims) {
   // cache that contains (lm mu)S basis states
   // ith element contains dim[(l m)S 2J=i], which is a small number
   // ==> uint32_t is enough
   std::map<std::array<uint8_t, 3>, std::vector<uint32_t>> su3su2_dims_map;

   for (int nhw = 0; nhw < nhw_su3su2_mult_maps.size(); nhw++) {
      for (const auto& su3su2_mult : nhw_su3su2_mult_maps[nhw]) {
         const auto& ss_mult = su3su2_mult.second;
         uint8_t lm = su3su2_mult.first[0];
         uint8_t mu = su3su2_mult.first[1];

         for (uint8_t ss = 0; ss < ss_mult.size(); ++ss) {
            // here I choose "auto" as this type may be changing to unsigned__int128
            auto mult = ss_mult[ss];
            if (mult) {
               std::array<uint8_t, 3> su3su2 = {lm, mu, ss};
               auto su3su2_dims = su3su2_dims_map.find(su3su2);
               if (su3su2_dims == su3su2_dims_map.end()) {
                  std::vector<uint32_t> dims;
                  computeDims(su3su2, dims);
                  su3su2_dims_map[su3su2] = dims;
                  for (size_t i = 0; i < dims.size(); ++i) {
                     nhw_dims[nhw][i] += mult * (uint64_t)dims[i];
                  }
               } else {
                  for (size_t jj = 0; jj < su3su2_dims->second.size(); ++jj) {
                     nhw_dims[nhw][jj] += mult * (uint64_t)su3su2_dims->second[jj];
                  }
               }
            }
         }
      }
   }
}

void ShowDimensions(std::ofstream& ofile, int Nmax,
                    const std::vector<std::array<uint64_t, 256>> nhw_jj_dim) {
   // Compute m-scheme dimensions both parities, i.e. Nmax = 0, 2, ... and Nmax = 1, 3, ... spaces

   // [Nmax] ---> m-scheme dimension of Nmax model space
   // 0 ... Nmax => Nmax + 1 elements
   std::vector<uint64_t> nmax_mscheme_dim(Nmax + 1, 0);

   nmax_mscheme_dim[0] = std::accumulate(nhw_jj_dim[0].begin(), nhw_jj_dim[0].end(), (uint64_t)0);
   for (size_t nmax = 2; nmax <= Nmax; nmax += 2) {
      nmax_mscheme_dim[nmax] =
          nmax_mscheme_dim[nmax - 2] +
          std::accumulate(nhw_jj_dim[nmax].begin(), nhw_jj_dim[nmax].end(), (uint64_t)0);
   }

   if (Nmax) {
      nmax_mscheme_dim[1] =
          std::accumulate(nhw_jj_dim[1].begin(), nhw_jj_dim[1].end(), (uint64_t)0);
   }

   for (size_t nmax = 3; nmax <= Nmax; nmax += 2) {
      nmax_mscheme_dim[nmax] =
          nmax_mscheme_dim[nmax - 2] +
          std::accumulate(nhw_jj_dim[nmax].begin(), nhw_jj_dim[nmax].end(), (uint64_t)0);
   }

   ofile << "########################################################" << std::endl;
   ofile << "Dimension of M-scheme basis" << std::endl;
   ofile << "########################################################" << std::endl;
   ofile << "Positive parity model spaces:" << std::endl;
   for (size_t nmax = 0; nmax <= Nmax; nmax += 2) {
      ofile << "Nmax = " << nmax << "\t\t";
      ofile << nmax_mscheme_dim[nmax] << std::endl;
   }

   if (Nmax) {
      ofile << std::endl << std::endl << "Negative parity model spaces:" << std::endl;
   }
   for (size_t nmax = 1; nmax <= Nmax; nmax += 2) {
      ofile << "Nmax = " << nmax << "\t\t";
      ofile << nmax_mscheme_dim[nmax] << std::endl;
   }

   ofile << std::endl
         << std::endl
         << "########################################################" << std::endl;
   ofile << "Dimensions of J-coupled basis" << std::endl;
   ofile << "########################################################" << std::endl;
   ofile << "Positive parity model spaces:" << std::endl;

   std::vector<std::array<uint64_t, 256>> nmax_jj_dim(Nmax + 1);
   nmax_jj_dim[0] = nhw_jj_dim[0];
   for (size_t nmax = 2; nmax <= Nmax; nmax += 2) {
      for (size_t jj = 0; jj < nmax_jj_dim[0].size(); ++jj) {
         nmax_jj_dim[nmax][jj] = nmax_jj_dim[nmax - 2][jj] + nhw_jj_dim[nmax][jj];
      }
   }

   if (Nmax) {
      nmax_jj_dim[1] = nhw_jj_dim[1];
   }
   for (size_t nmax = 3; nmax <= Nmax; nmax += 2) {
      for (size_t jj = 0; jj < nmax_jj_dim[0].size(); ++jj) {
         nmax_jj_dim[nmax][jj] = nmax_jj_dim[nmax - 2][jj] + nhw_jj_dim[nmax][jj];
      }
   }

   for (size_t nmax = 0; nmax <= Nmax; nmax += 2) {
      ofile << "******  Nmax = " << nmax << "  ******" << std::endl;
      uint64_t total = 0;
      for (size_t jj = 0; jj < nmax_jj_dim[nmax].size(); ++jj) {
         uint64_t jjdim = nmax_jj_dim[nmax][jj];
         if (jjdim) {
            char szLine[] = "J =                 ";

            std::ostringstream sstream;
            if (jj % 2) {
               sstream << jj << "/2";
            } else {
               sstream << (jj / 2);
            }
            strncpy((szLine + 4), sstream.str().c_str(), sstream.str().length());
            ofile << szLine << jjdim << std::endl;

            total += jjdim;
         }
      }
      ofile << "----------------------------------------" << std::endl;
      ofile << "Total:              " << total << std::endl << std::endl << std::endl;
   }

   for (size_t nmax = 1; nmax <= Nmax; nmax += 2) {
      ofile << "******  Nmax = " << nmax << "  ******" << std::endl;
      uint64_t total = 0;
      for (size_t jj = 0; jj < nmax_jj_dim[nmax].size(); ++jj) {
         uint64_t jjdim = nmax_jj_dim[nmax][jj];
         if (jjdim) {
            char szLine[] = "J =                 ";

            std::ostringstream sstream;
            if (jj % 2) {
               sstream << jj << "/2";
            } else {
               sstream << (jj / 2);
            }
            strncpy((szLine + 4), sstream.str().c_str(), sstream.str().length());
            ofile << szLine << jjdim << std::endl;

            total += jjdim;
         }
      }
      ofile << "----------------------------------------" << std::endl;
      ofile << "Total:              " << total << std::endl << std::endl << std::endl;
   }
}

std::tuple<int, int, int, int> LoadModelSpaceDefinition(
    const std::string& filename, std::vector<int>& nhw_spaces,
    MODEL_SPACE_DEFINITION& nhw_map_sspn_to_map_ss_to_set_su3) {
   // structure for messages due to errors encountered during reading model space file
   std::ostringstream error_message;

   uint8_t ssp, ssn, ss, lm, mu;
   bool without_output = false;

   std::ifstream input_file(filename.c_str());
   if (!input_file) {
      error_message << "\nCould not open model space definition file '" << filename << "'.\n";
      throw std::runtime_error(error_message.str());
   }

   if (without_output == false)
      std::cout << "Loading definition of the model space from '" << filename << "'" << std::endl;

   int tmp;
   int N, num_protons, num_neutrons, JJcut;
   input_file >> num_protons >> num_neutrons >> JJcut;
   if (num_protons <= 0) {
      throw("\nError: number of protons must to be a positive integer!\n");
   }
   if (num_neutrons <= 0) {
      throw("\nError: number of neutrons must to be a positive integer!\n");
   }
   if (JJcut < -1 || JJcut > 255) {
      error_message << "\nTotal angular momentum value is out of bound! 2J:" << JJcut
                    << ". Allowed range for 2J values: 1 <= 2J <= 255!\n";
      throw std::runtime_error(error_message.str());
   }

   if (without_output == false)
      std::cout << "Number of protons: " << num_protons << "\t Number of neutrons:" << num_neutrons
                << "\t2J:" << JJcut << std::endl;

   while (true) {
      input_file >> N;
      // Check if we reached the end of file
      if (!input_file) break;

      if (without_output == false) std::cout << "Nhw = " << N << " ";

      int number_SpSn;
      input_file >> number_SpSn;

      if (number_SpSn < -1 || number_SpSn == 0) {
         error_message << "\nNumber of 2Sp 2Sn pairs for " << N
                       << "hw subspace must be either equal to -1 or a positive integer! "
                       << number_SpSn << "!\n";
         throw std::runtime_error(error_message.str());
      }

      std::map<std::array<uint8_t, 2>, std::map<uint8_t, std::set<std::array<uint8_t, 2>>>>
          map_sspn_to_map_ss_to_set_su3;
      if (number_SpSn >= +1) {
         if (without_output == false) std::cout << number_SpSn << std::endl;

         for (size_t i = 0; i < number_SpSn; ++i) {
            input_file >> tmp;
            // end of file should not occur here!
            if (!input_file) {
               error_message
                   << "\nWrong structure of model space file ... another Sp Sn pair expected!\n";
               throw std::runtime_error(error_message.str());
            }
            ssp = tmp;
            if (ssp > num_protons || (ssp % 2 != num_protons % 2)) {
               error_message << "A given value of total proton spin 2Sp:" << (int)ssp
                             << " is not physically allowed!" << std::endl;
               throw std::runtime_error(error_message.str());
            }

            input_file >> tmp;
            // end of file should not occur here!
            if (!input_file) {
               error_message
                   << "\nWrong structure of model space file ... another Sp Sn pair expected!\n";
               throw std::runtime_error(error_message.str());
            }
            ssn = tmp;
            if (ssn > num_neutrons || (ssn % 2 != num_neutrons % 2)) {
               error_message << "A given value of total neutron spin 2Sp:" << (int)ssn
                             << " is not physically allowed!" << std::endl;
               throw std::runtime_error(error_message.str());
            }

            int number_S;
            input_file >> number_S;
            if (!input_file) {
               error_message << "\nWrong structure of model space file ... number of different 2S "
                                "values for SSp:"
                             << (int)ssp << " SSn:" << (int)ssn << " expected!\n";
               throw std::runtime_error(error_message.str());
            }

            if (number_S < -1 || number_S == 0) {
               error_message << "\nNumber of 2S values for " << N
                             << "hw subspace and 2Sp:" << (int)ssp << " 2Sn:" << (int)ssn
                             << " must be equal to -1 or a positive integer!\n";
               throw std::runtime_error(error_message.str());
            }

            if (without_output == false)
               std::cout << "2Sp=" << (int)ssp << " 2Sn=" << (int)ssn
                         << " #(allowed Sp x Sn-->S)=" << number_S << std::endl;

            std::map<uint8_t, std::set<std::array<uint8_t, 2>>> map_ss_to_set_su3;
            if (number_S >= 1) {
               for (size_t i = 0; i < number_S; ++i) {
                  std::set<std::array<uint8_t, 2>> allowed_su3;

                  input_file >> tmp;
                  // end of file should not occur here!
                  if (!input_file) {
                     error_message << "\nWrong structure of model space file!\n";
                     throw std::runtime_error(error_message.str());
                  }
                  ss = tmp;
                  if (ss < abs(ssp - ssn) || ss > ssp + ssn) {
                     error_message << "\nUnphysical value of total intrinsic spin 2S:" << (int)ss
                                   << "! Coupling 2Sp:" << (int)ssp << " x 2Sn:" << (int)ssn
                                   << " does not yield 2S:" << (int)ss << ".\n";
                     throw std::runtime_error(error_message.str());
                  }

                  int number_SU3;
                  input_file >> number_SU3;
                  if (!input_file) {
                     error_message
                         << "\nWrong structure of model space file .. number of SU(3) irreps for "
                         << N << "hw SSp:" << (int)ssp << " SSn:" << (int)ssn << " SS:" << (int)ss
                         << " expected!\n";
                     throw std::runtime_error(error_message.str());
                  }

                  if (number_SU3 < -1 || number_SU3 == 0) {
                     error_message << "\nNumber of SU(3) irrep labels for " << N
                                   << "hw subspace and 2Sp:" << (int)ssp << " 2Sn:" << (int)ssn
                                   << " 2S:" << (int)ss
                                   << " must be equal either to -1 or to a positive integer!\n";
                     throw std::runtime_error(error_message.str());
                  }

                  if (without_output == false)
                     std::cout << "2S=" << (int)ss << " #(allowed SU3 irreps)=" << number_SU3
                               << std::endl;

                  if (number_SU3 >= +1) {
                     for (size_t i = 0; i < number_SU3; ++i) {
                        input_file >> tmp;
                        // end of file should not occur here!
                        if (!input_file) {
                           error_message << "\nWrong structure of model space file ... another "
                                            "SU(3) irrep expected! ["
                                         << N << "hw SSp:" << (int)ssp << " SSn:" << (int)ssn
                                         << " SS:" << (int)ss << "]\n";
                           throw std::runtime_error(error_message.str());
                        }
                        if (tmp < 0 || tmp > 255) {
                           error_message << "\nWrong number of SU(3) label lambda:" << tmp << "!\n";
                           throw std::runtime_error(error_message.str());
                        }
                        lm = tmp;
                        input_file >> tmp;
                        if (!input_file) {
                           error_message << "\nWrong structure of model space file ... another "
                                            "SU(3) irrep expected! ["
                                         << N << "hw SSp:" << (int)ssp << " SSn:" << (int)ssn
                                         << " SS:" << (int)ss << "]\n";
                           throw std::runtime_error(error_message.str());
                        }

                        if (tmp < 0 || tmp > 255) {
                           error_message << "\nWrong number of SU(3) label mu:" << tmp << "!\n";
                           throw std::runtime_error(error_message.str());
                        }
                        mu = tmp;
                        // we may reached
                        if (without_output == false)
                           std::cout << "(" << (int)lm << " " << (int)mu << ")\t";
                        allowed_su3.emplace(std::array<uint8_t, 2>({lm, mu}));
                     }
                  }

                  map_ss_to_set_su3.emplace(ss, allowed_su3);
                  if (without_output == false) std::cout << std::endl;
               }
            }
            map_sspn_to_map_ss_to_set_su3.emplace(std::array<uint8_t, 2>({ssp, ssn}),
                                                  map_ss_to_set_su3);
         }
      } else {
         if (without_output == false) std::cout << "full subspace" << std::endl;
      }

      nhw_spaces.push_back(N);
      nhw_map_sspn_to_map_ss_to_set_su3.push_back(std::move(map_sspn_to_map_ss_to_set_su3));
   }
   return std::tuple<int, int, int, int>(num_protons, num_neutrons, nhw_spaces.back(), JJcut);
}

void ShowModelSpace(const std::vector<int>& nhw_spaces,
                    const MODEL_SPACE_DEFINITION& nhw_map_sspn_to_map_ss_to_set_su3) {
   for (size_t i = 0; i < nhw_spaces.size(); ++i) {
      std::cout << nhw_spaces[i] << " ";
      const auto& map_sspn_to_map_ss_to_set_su3 = nhw_map_sspn_to_map_ss_to_set_su3[i];
      if (map_sspn_to_map_ss_to_set_su3.empty()) {
         std::cout << "-1" << std::endl;
         continue;
      }
      std::cout << map_sspn_to_map_ss_to_set_su3.size() << std::endl;

      for (const auto& sspn_to_map_ss_to_set_su3 : map_sspn_to_map_ss_to_set_su3) {
         const auto& sspn = sspn_to_map_ss_to_set_su3.first;
         std::cout << "\t" << (int)sspn[0] << " " << (int)sspn[1] << " ";
         const auto& map_ss_to_set_su3 = sspn_to_map_ss_to_set_su3.second;
         if (map_ss_to_set_su3.empty()) {
            std::cout << "-1" << std::endl;
            continue;
         }

         std::cout << map_ss_to_set_su3.size() << std::endl;
         for (const auto& ss_to_set_su3 : map_ss_to_set_su3) {
            auto ss = ss_to_set_su3.first;
            std::cout << "\t\t" << (int)ss << " ";
            const auto& set_su3 = ss_to_set_su3.second;
            if (set_su3.empty()) {
               std::cout << "-1" << std::endl;
               continue;
            }

            std::cout << set_su3.size() << std::endl;
            for (const auto& su3 : set_su3) {
               std::cout << "\t\t\t" << (int)su3[0] << " " << (int)su3[1] << std::endl;
            }
         }
      }
   }
}

void SelectU3SU2Irreps(
    const std::vector<int>& nhw_spaces,
    const std::vector<std::map<std::array<uint8_t, 3>, std::map<std::array<uint8_t, 2>, uint64_t>>>&
        nhw_map_spins_to_map_su3_to_mult,
    const MODEL_SPACE_DEFINITION& nhw_map_sspn_to_map_ss_to_set_su3,
    std::vector<std::pair<std::array<uint8_t, 6>, uint64_t>>& selected_u3su2_mult) {
   for (size_t inhw = 0; inhw < nhw_spaces.size(); ++inhw) {
      uint8_t nhw = nhw_spaces[inhw];
      // if empty ==> nhw -1 ==> select all nhw U(3)xSU(2) irreps
      if (nhw_map_sspn_to_map_ss_to_set_su3[inhw].empty()) {
         for (const auto& spins_to_map_su3_to_mult : nhw_map_spins_to_map_su3_to_mult[inhw]) {
            const auto& spins = spins_to_map_su3_to_mult.first;
            const auto& map_su3_to_mult = spins_to_map_su3_to_mult.second;
            for (const auto& su3_to_mult : map_su3_to_mult) {
               const auto& su3 = su3_to_mult.first;
               uint64_t mult = su3_to_mult.second;
               selected_u3su2_mult.emplace_back(
                   std::array<uint8_t, 6>({nhw, spins[0], spins[1], spins[2], su3[0], su3[1]}),
                   mult);
            }
         }
         continue;
      }

      // obtain nhw subspace of model space
      const auto& map_sspn_to_map_ss_to_set_su3 = nhw_map_sspn_to_map_ss_to_set_su3[inhw];
      for (const auto& spins_to_map_su3_to_mult : nhw_map_spins_to_map_su3_to_mult[inhw]) {
         const auto& spins = spins_to_map_su3_to_mult.first;
         const auto sspn_to_map_ss_to_set_su3_iter =
             map_sspn_to_map_ss_to_set_su3.find({spins[0], spins[1]});
         if (sspn_to_map_ss_to_set_su3_iter == map_sspn_to_map_ss_to_set_su3.end()) {
            // a given {2Sp, 2Sn} pair in not inluded in model space ==> skip it
            continue;
         }

         // {2Sp, 2Sn} found in model space
         // 2Sp 2Sn -1 ==> include all configurations
         if (sspn_to_map_ss_to_set_su3_iter->second.empty()) {
            const auto& map_su3_to_mult = spins_to_map_su3_to_mult.second;
            for (const auto& su3_to_mult : map_su3_to_mult) {
               const auto& su3 = su3_to_mult.first;
               uint64_t mult = su3_to_mult.second;
               selected_u3su2_mult.emplace_back(
                   std::array<uint8_t, 6>({nhw, spins[0], spins[1], spins[2], su3[0], su3[1]}),
                   mult);
            }
            continue;
         }

         // {2Sp, 2Sn} has a set of {2S} values selected
         const auto ss_to_set_su3_iter = sspn_to_map_ss_to_set_su3_iter->second.find(spins[2]);
         if (ss_to_set_su3_iter == sspn_to_map_ss_to_set_su3_iter->second.end()) {
            // a given SS is not in model space => skip
            continue;
         }

         if (ss_to_set_su3_iter->second.empty()) {
            // set of su3 is empty ==> SS -1 ==> include all su3
            const auto& map_su3_to_mult = spins_to_map_su3_to_mult.second;
            for (const auto& su3_to_mult : map_su3_to_mult) {
               const auto& su3 = su3_to_mult.first;
               uint64_t mult = su3_to_mult.second;
               selected_u3su2_mult.emplace_back(
                   std::array<uint8_t, 6>({nhw, spins[0], spins[1], spins[2], su3[0], su3[1]}),
                   mult);
            }
            continue;
         }

         // set of selected SU(3) irreps in model space with nhw 2Sp 2Sn 2S quantum numbers
         const auto& su3_model_space = ss_to_set_su3_iter->second;
         const auto& map_su3_to_mult = spins_to_map_su3_to_mult.second;
         for (const auto& su3_to_mult : map_su3_to_mult) {
            const auto& su3 = su3_to_mult.first;
            if (su3_model_space.find(su3) != su3_model_space.end()) {
               uint64_t mult = su3_to_mult.second;
               selected_u3su2_mult.emplace_back(
                   std::array<uint8_t, 6>({nhw, spins[0], spins[1], spins[2], su3[0], su3[1]}),
                   mult);
            }
         }
      }
   }
}

void ShowDimensions(
    const std::string model_space_name, std::ofstream& ofile, const std::vector<int>& nhw_spaces,
    const std::vector<std::pair<std::array<uint8_t, 6>, uint64_t>>& selected_u3su2_mult,
    int JJcut) {
   std::array<uint64_t, 256> total_dimensions({});

   int old_nhw = -1;

   std::ostringstream sstream;
   sstream << "Dimensions of U(3) subspaces spanning model space '" << model_space_name << "'.";
   size_t nchars = sstream.tellp();
   ofile << std::string(nchars, '-') << std::endl;
   ofile << sstream.str() << std::endl;
   ofile << std::string(nchars, '-') << std::endl;

   for (const auto& u3su2_mult : selected_u3su2_mult) {
      int nhw = u3su2_mult.first[0];
      int ssp = u3su2_mult.first[1];
      int ssn = u3su2_mult.first[2];
      int ss = u3su2_mult.first[3];
      int lm = u3su2_mult.first[4];
      int mu = u3su2_mult.first[5];
      uint64_t mult = u3su2_mult.second;
      if (old_nhw != nhw) {
         ofile << nhw << "hw" << std::endl;
         old_nhw = nhw;
      }

      std::array<uint8_t, 3> su3su2 = {lm, mu, ss};
      std::vector<uint32_t> dims;
      computeDims(su3su2, dims);

      if (JJcut == -1) {  // => show dimensions for all values of J
         ofile << "\t\t" << ssp << " " << ssn << " " << ss << "\t(" << lm << " " << mu << ")\t"
               << mult << std::endl;
         for (size_t jj = 0; jj < dims.size(); ++jj) {
            if (dims[jj]) {
               total_dimensions[jj] += mult * (uint64_t)dims[jj];
               ofile << "\t\t\t\t\t\tJ=";
               if (jj % 2) {
                  ofile << jj << "/2";
               } else {
                  ofile << (jj / 2);
               }
               ofile << "\t" << mult * dims[jj] << std::endl;
            }
         }
      } else {
         ofile << "\t\t" << ssp << " " << ssn << " " << ss << "\t(" << lm << " " << mu << ")\t\t";
         if (JJcut < dims.size() && dims[JJcut]) {
            total_dimensions[JJcut] += mult * (uint64_t)dims[JJcut];
            ofile << mult * dims[JJcut] << std::endl;
         } else {
            ofile << "0" << std::endl;
         }
      }
   }

   ofile << std::endl;
   sstream.str("");
   sstream.clear();
   sstream << "Total dimension of model space '" << model_space_name << "'.";
   nchars = sstream.tellp();
   ofile << std::string(nchars, '-') << std::endl;
   ofile << sstream.str() << std::endl;
   ofile << std::string(nchars, '-') << std::endl;

   if (JJcut == -1) {
      for (int jj = 0; jj < 256; ++jj) {
         if (total_dimensions[jj]) {
            ofile << "J=";
            if (jj % 2) {
               ofile << jj << "/2";
            } else {
               ofile << (jj / 2);
            }
            ofile << "\t" << total_dimensions[jj] << std::endl;
         }
      }
   } else {
      if (total_dimensions[JJcut]) {
         ofile << "J=";
         if (JJcut % 2) {
            ofile << JJcut << "/2";
         } else {
            ofile << (JJcut / 2);
         }
         ofile << "\t" << total_dimensions[JJcut] << std::endl;
      }
   }
}

int main(int argc, char** argv) {
   // number of protons
   int num_protons;
   // number of neutrons
   int num_neutrons;
   int Nmax;
   int JJcut;

   std::vector<int> nhw_spaces;
   MODEL_SPACE_DEFINITION nhw_map_sspn_to_map_ss_to_set_su3;

   if (argc == 1) {
      std::cout << "Enter the number of protons  ... ";
      std::cin >> num_protons;
      std::cout << "Enter the number of neutrons ... ";
      std::cin >> num_neutrons;
      std::cout << "Enter the maximal model space size Nmax=";
      std::cin >> Nmax;

      nhw_spaces.resize(Nmax + 1);
      // nhw_spaces = {0, 1, 2, .... , Nmax}
      std::iota(nhw_spaces.begin(), nhw_spaces.end(), 0);
   } else if (argc == 2) {
      std::string model_space_file(argv[1]);
      try {
         std::tie(num_protons, num_neutrons, Nmax, JJcut) = LoadModelSpaceDefinition(
             model_space_file, nhw_spaces, nhw_map_sspn_to_map_ss_to_set_su3);
      } catch (const std::runtime_error& error) {
         std::cerr << error.what() << std::endl;
         return EXIT_FAILURE;
      }
      ShowModelSpace(nhw_spaces, nhw_map_sspn_to_map_ss_to_set_su3);
   } else {
      std::cout << "Usage: " << argv[0] << " <model space>" << std::endl;
      return EXIT_FAILURE;
   }

   // nhw_distributions[Nhw]: vector of fermion distributions that span Nhw subspace
   std::vector<std::vector<Distribution>> nhw_proton_distributions, nhw_neutron_distributions;
   Distribution max_occupancy_proton, max_occupancy_neutron;
   nhw_proton_distributions.resize(Nmax + 1);
   nhw_neutron_distributions.resize(Nmax + 1);

   GenerateDistributions(Nmax, num_protons, nhw_proton_distributions, max_occupancy_proton);
   GenerateDistributions(Nmax, num_neutrons, nhw_neutron_distributions, max_occupancy_neutron);

   //   ShowDistributions(nhw_proton_distributions);

   std::vector<std::vector<UN::SU3xSU2_VEC>> n_A_irreps;

   std::cout << "Generating allowed U(N)xSU(2)>SU(3)xSU(2) irreps ..." << std::endl;
   if (num_protons > num_neutrons) {
      GenerateSingleShellIrreps(max_occupancy_proton, n_A_irreps);
   } else {
      GenerateSingleShellIrreps(max_occupancy_neutron, n_A_irreps);
   }
   std::cout << "Done" << std::endl;

   //  ShowSingleShellIrreps(n_A_irreps);

   std::cout << "Evaluating allowed proton configurations  ..." << std::endl;
   std::vector<std::vector<std::pair<std::array<uint8_t, 2>, std::vector<uint32_t>>>>
       nhw_proton_irreps;
   nhw_proton_irreps.resize(Nmax + 1);
   int maxSS = num_protons;
   GenerateSU3xSU2Irreps(maxSS, nhw_proton_distributions, n_A_irreps, nhw_proton_irreps);
   std::cout << "Done" << std::endl;

   std::vector<std::vector<std::pair<std::array<uint8_t, 2>, std::vector<uint32_t>>>>
       nhw_neutron_irreps;
   nhw_neutron_irreps.resize(Nmax + 1);
   if (num_protons != num_neutrons) {
      std::cout << "Evaluating allowed neutron configurations ..." << std::endl;
      maxSS = num_neutrons;
      GenerateSU3xSU2Irreps(maxSS, nhw_neutron_distributions, n_A_irreps, nhw_neutron_irreps);
   } else {
      nhw_neutron_irreps = nhw_proton_irreps;
   }
   std::cout << "Done" << std::endl;

   std::cout << "Coupling proton and neutron configurations ...\n";
   std::cout.flush();

   maxSS = num_protons + num_neutrons;
   bool fullNmaxModelSpace = (argc == 1);
   if (fullNmaxModelSpace) {
      std::vector<std::map<std::array<uint8_t, 2>, std::vector<uint64_t>>> nhw_su3su2_mult_maps;
      nhw_su3su2_mult_maps.resize(Nmax + 1);
      CoupleProtonsNeutrons(maxSS, nhw_proton_irreps, nhw_neutron_irreps, nhw_su3su2_mult_maps);
      std::vector<std::array<uint64_t, 256>> nhw_dims;
      nhw_dims.resize(Nmax + 1);
      ComputeDimensions(nhw_su3su2_mult_maps, nhw_dims);

      std::ofstream file;
      file.open("dimensions.log", std::ios::out);
      ShowDimensions(file, Nmax, nhw_dims);
   } else {
      std::vector<std::map<std::array<uint8_t, 3>, std::map<std::array<uint8_t, 2>, uint64_t>>>
          nhw_map_spins_to_map_su3_to_mult;

      nhw_map_spins_to_map_su3_to_mult.resize(Nmax + 1);

      CoupleProtonsNeutrons(maxSS, nhw_proton_irreps, nhw_neutron_irreps, nhw_spaces,
                            nhw_map_spins_to_map_su3_to_mult);
      std::vector<std::pair<std::array<uint8_t, 6>, uint64_t>> selected_u3su2_mult;
      SelectU3SU2Irreps(nhw_spaces, nhw_map_spins_to_map_su3_to_mult,
                        nhw_map_sspn_to_map_ss_to_set_su3, selected_u3su2_mult);
      std::ofstream file;
      file.open("dimensions.log", std::ios::out);
      ShowDimensions(argv[1], file, nhw_spaces, selected_u3su2_mult, JJcut);
   }
   return EXIT_SUCCESS;
}
