CC=g++
CCFLAGS = -std=c++14 -O3 -c
#CCFLAGS = -std=c++14 -g -c

default: dimensionsNCSM
		
main.o: main.cpp
	   $(CC) $(CCFLAGS) main.cpp

dimensionsNCSM: main.o 
	   $(CC) -o dimensionsNCSM main.o 
clean:
	   rm -f *.o
	   rm -f *.log
	   rm -f *~
	   rm -f dimensionsNCSM
